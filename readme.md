## Fake Wallet SOAP Service

### Installation

#### Using Docker Compose

```
git clone git@bitbucket.org:alejobit/fake-wallet-soap.git
cd fake-wallet-soap
cp .env.dist .env
docker-compose up -d
docker-compose exec php-fpm composer install
docker-compose exec php-fpm php bin/console doctrine:database:create
docker-compose exec php-fpm php bin/console doctrine:schema:create
```

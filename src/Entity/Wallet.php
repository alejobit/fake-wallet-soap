<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WalletRepository")
 */
class Wallet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $balance;

    public function __construct()
    {
        $this->balance = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function increaseBalance(float $value): self
    {
        $this->balance += $value;

        return $this;
    }

    public function decreaseBalance(float $value): self
    {
        $this->balance -= $value;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 */
class Payment
{
    const STATUS_UNCONFIRMED = 'UNCOMFIRMED';
    const STATUS_CONFIRMED = 'COMFIRMED';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="payments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    public function __construct(float $value, Customer $customer)
    {
        $this->value = $value;
        $this->status = self::STATUS_UNCONFIRMED;
        $this->token = substr(uniqid(), -6);
        $this->customer = $customer;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function confirm(): self
    {
        $this->status = self::STATUS_CONFIRMED;

        return $this;
    }

    public function isConfirmed(): bool
    {
        return $this->status === self::STATUS_CONFIRMED;
    }
}

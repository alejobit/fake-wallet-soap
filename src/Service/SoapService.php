<?php

namespace App\Service;

use App\Entity\Customer;
use App\Entity\Payment;
use App\Helper\JSend;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SoapService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param \Swift_Mailer $mailer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        \Swift_Mailer $mailer
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->mailer = $mailer;
    }

    /**
     * Customer registration
     *
     * @param string $document
     * @param string $fullName
     * @param string $email
     * @param string $mobilePhone
     * @return array
     */
    public function customerRegistration($document, $fullName, $email, $mobilePhone)
    {
        $customer = (new Customer())
            ->setDocument($document)
            ->setFullName($fullName)
            ->setEmail($email)
            ->setMobilePhone($mobilePhone);

        $errors = $this->validator->validate($customer);

        if ($errors->count()) {
            $errorMessages = [];

            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return JSend::fail($errorMessages)->toArray();
        }

        $this->entityManager->persist($customer);

        try {
            $this->entityManager->flush();

            return JSend::success(
                compact('customer'),
                'The client was successfully registered'
            )->toArray();
        } catch (\Exception $exception) {
            return JSend::error($exception->getMessage(), $exception->getCode())->toArray();
        }
    }

    /**
     * Recharge wallet
     *
     * @param string $document
     * @param string $mobilePhone
     * @param float $value
     * @return array
     */
    public function rechargeWallet($document, $mobilePhone, $value)
    {
        $constraints = new Collection([
            'document' => [
                new NotBlank(),
                new Length(['max' => 32]),
            ],
            'mobilePhone' => [
                new NotBlank(),
                new Length(['max' => 32]),
            ],
            'value' => [
                new NotBlank(),
                new Type(['type' => 'float']),
                new GreaterThan(['value' => 0]),
            ],
        ]);

        $errors = $this->validator->validate(
            compact('document', 'mobilePhone', 'value'),
            $constraints
        );

        if ($errors->count()) {
            $errorMessages = [];

            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return JSend::fail($errorMessages)->toArray();
        }

        /** @var Customer|null $customer */
        $customer = $this->entityManager
            ->getRepository(Customer::class)
            ->findOneBy(compact('document', 'mobilePhone'));

        if (is_null($customer)) {
            return JSend::fail(
                compact('document', 'mobilePhone'),
                'No matching client was found'
            )->toArray();
        }

        $customer->getWallet()->increaseBalance($value);
        $this->entityManager->flush();

        return JSend::success([
            'balance' => $customer->getWallet()->getBalance(),
        ], 'The wallet was successfully recharged')->toArray();
    }

    /**
     * Check wallet balance
     *
     * @param string $document
     * @param string $mobilePhone
     * @return array
     */
    public function checkBalance($document, $mobilePhone)
    {
        $constraints = new Collection([
            'document' => [
                new NotBlank(),
                new Length(['max' => 32]),
            ],
            'mobilePhone' => [
                new NotBlank(),
                new Length(['max' => 32]),
            ],
        ]);

        $errors = $this->validator->validate(
            compact('document', 'mobilePhone'),
            $constraints
        );

        if ($errors->count()) {
            $errorMessages = [];

            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return JSend::fail($errorMessages)->toArray();
        }

        /** @var Customer|null $customer */
        $customer = $this->entityManager
            ->getRepository(Customer::class)
            ->findOneBy(compact('document', 'mobilePhone'));

        if (is_null($customer)) {
            return JSend::fail(
                compact('document', 'mobilePhone'),
                'No matching client was found'
            )->toArray();
        }

        return JSend::success([
            'balance' => $customer->getWallet()->getBalance(),
        ])->toArray();
    }

    /**
     * Payment
     *
     * @param string $document
     * @param string $mobilePhone
     * @param float $value
     * @return array
     */
    public function payment($document, $mobilePhone, $value)
    {
        $constraints = new Collection([
            'document' => [
                new NotBlank(),
                new Length(['max' => 32]),
            ],
            'mobilePhone' => [
                new NotBlank(),
                new Length(['max' => 32]),
            ],
            'value' => [
                new NotBlank(),
                new Type(['type' => 'float']),
                new GreaterThan(['value' => 0]),
            ],
        ]);

        $errors = $this->validator->validate(
            compact('document', 'mobilePhone', 'value'),
            $constraints
        );

        if ($errors->count()) {
            $errorMessages = [];

            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return JSend::fail($errorMessages)->toArray();
        }

        /** @var Customer|null $customer */
        $customer = $this->entityManager
            ->getRepository(Customer::class)
            ->findOneBy(compact('document', 'mobilePhone'));

        if (is_null($customer)) {
            return JSend::fail(
                compact('document', 'mobilePhone'),
                'No matching client was found'
            )->toArray();
        }

        $payment = new Payment($value, $customer);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $message = (new \Swift_Message())
            ->setSubject('Fake Wallet Payment Request')
            ->setFrom(
                (string)getenv('APP_MAILER_FROM_ADDRESS'),
                (string)getenv('APP_MAILER_FROM_NAME')
            )
            ->setTo($customer->getEmail(), $customer->getFullName())
            ->setBody(
                'Hello! Your confirmation token is: ' . $payment->getToken(),
                'text/plain'
            );

        $this->mailer->send($message);

        return JSend::success([
            'sessionId' => $payment->getId(),
        ], 'An email has been sent with the confirmation token')->toArray();
    }

    /**
     * @param int $sessionId
     * @param string $token
     * @return array
     */
    public function confirmPayment($sessionId, $token)
    {
        $constraints = new Collection([
            'sessionId' => [
                new NotBlank(),
                new Type(['type' => 'int']),
            ],
            'token' => [
                new NotBlank(),
                new Length(['min' => 6, 'max' => 6]),
            ],
        ]);

        $errors = $this->validator->validate(
            compact('sessionId', 'token'),
            $constraints
        );

        if ($errors->count()) {
            $errorMessages = [];

            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return JSend::fail($errorMessages)->toArray();
        }

        /** @var Payment|null $payment */
        $payment = $this->entityManager
            ->getRepository(Payment::class)
            ->findOneBy([
                'id' => $sessionId,
                'token' => $token,
            ]);

        if (is_null($payment)) {
            return JSend::fail(
                compact('sessionId', 'token'),
                'No matching payment was found'
            )->toArray();
        }

        if ($payment->isConfirmed()) {
            return JSend::fail(
                compact('sessionId', 'token'),
                'The payment has already been confirmed'
            )->toArray();
        }

        $payment->confirm();
        $payment->getCustomer()->getWallet()->decreaseBalance($payment->getValue());
        $this->entityManager->flush();

        return JSend::success(
            compact('sessionId', 'token'),
            'Payment confirmed successfully'
        )->toArray();
    }
}

<?php

namespace App\Controller;

use App\Service\SoapService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Server;
use Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence;

class SoapController extends Controller
{
    /**
     * @param SoapService $soapService
     * @return Response
     */
    public function index(SoapService $soapService)
    {
        $soapServer = (new Server())
            ->setWSDL($this->generateUrl('wsdl', [], UrlGeneratorInterface::ABSOLUTE_URL))
            ->setObject($soapService)
            ->setReturnResponse();

        $soapResponse = $soapServer->handle();

        return new Response($soapResponse, 200, [
            'Content-Type' => 'text/xml',
        ]);
    }

    /**
     * @return Response
     */
    public function wsdl()
    {
        $autoDiscover = (new AutoDiscover(new ArrayOfTypeSequence()))
            ->setClass(SoapService::class)
            ->setUri($this->generateUrl('soap', [], UrlGeneratorInterface::ABSOLUTE_URL));

        return new Response($autoDiscover->generate()->toXML(), 200, [
            'Content-Type' => 'text/xml',
        ]);
    }
}
